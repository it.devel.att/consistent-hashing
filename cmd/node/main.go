package main

import (
	"consistent-hashing/pkg/nodes"
	transports "consistent-hashing/pkg/transports/redis"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/redis/go-redis/v9"
)

func main() {
	sigCh := make(chan os.Signal, 2)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer redisClient.Close()

	transport := transports.NewTransport(redisClient, transports.WithExpiration(time.Second*20))
	node, err := nodes.NewNode(
		nodes.WithTransport(transport),
		// Heartbeat should be aligned with transport keys expiration
		nodes.WithHeartbeat(time.Second*15),
	)
	if err != nil {
		panic(err)
	}

	err = node.Join()
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := node.Close(); err != nil {
			fmt.Println("err while trying to close node", err)
			return
		}
		fmt.Println("successfully close")
	}()

	// we can use it as NATS channel postfix here
	// node.ID
	fmt.Println("waiting to stop")
	<-sigCh
	fmt.Println("receive done signal")

}
