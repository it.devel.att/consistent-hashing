package main

import (
	"consistent-hashing/pkg/hashring"
	"consistent-hashing/pkg/observer"
	transports "consistent-hashing/pkg/transports/redis"
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	sigCh := make(chan os.Signal, 2)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer redisClient.Close()

	transport := transports.NewObserver(redisClient)

	hr := hashring.New(nil)
	ob, err := observer.New(hr, transport)
	if err != nil {
		panic(err)
	}
	defer ob.Stop()

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				time.Sleep(time.Millisecond * 200)
				randUUID, err := uuid.NewUUID()
				if err != nil {
					panic(err)
				}
				uuidStr := randUUID.String()

				node := hr.GetNode([]byte(uuidStr))
				fmt.Println(node)
			}
		}
	}()

	fmt.Println("waiting to stop")
	<-sigCh
	cancel()
	fmt.Println("receive done signal")

}
