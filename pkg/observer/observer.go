package observer

import (
	"consistent-hashing/pkg/hashring"
	"consistent-hashing/pkg/transports"
	"context"
	"fmt"
	"sync"
)

type Observer struct {
	ctx       context.Context
	cancel    context.CancelFunc
	wg        *sync.WaitGroup
	transport transports.Observer

	ring *hashring.Ring
}

func New(hr *hashring.Ring, t transports.Observer) (*Observer, error) {
	ringNodes, err := t.Nodes()
	if err != nil {
		return nil, err
	}
	hr.Set(ringNodes)
	ctx, cancel := context.WithCancel(context.Background())

	ob := &Observer{
		ctx:       ctx,
		cancel:    cancel,
		wg:        &sync.WaitGroup{},
		transport: t,
		ring:      hr,
	}
	ob.run()
	return ob, nil
}

func (o *Observer) run() {
	o.wg.Add(1)
	go func() {
		defer o.wg.Done()

		for {
			select {
			case <-o.ctx.Done():
				return
			case nodeID := <-o.transport.Joins():
				fmt.Println("node join: ", nodeID)
				o.ring.Join(nodeID)
			case nodeID := <-o.transport.Leaves():
				fmt.Println("node leave: ", nodeID)
				o.ring.Delete(nodeID)
			}
		}
	}()
}

func (o *Observer) Stop() {
	o.cancel()
	o.wg.Wait()
}
