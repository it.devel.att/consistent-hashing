package nodes

import (
	"consistent-hashing/pkg/transports"
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
)

const (
	defaultHeartbeat = time.Second * 15
)

type Node struct {
	ctx    context.Context
	cancel context.CancelFunc
	wg     sync.WaitGroup

	ID        string
	transport transports.Node
	heartbeat time.Duration
}

type Option func(n *Node)

func WithID(id string) Option {
	return func(n *Node) {
		n.ID = id
	}
}

func WithTransport(t transports.Node) Option {
	return func(n *Node) {
		n.transport = t
	}
}

func WithHeartbeat(v time.Duration) Option {
	return func(n *Node) {
		n.heartbeat = v
	}
}

func NewNode(opts ...Option) (*Node, error) {
	ctx, cancel := context.WithCancel(context.Background())
	n := &Node{
		ctx:       ctx,
		cancel:    cancel,
		heartbeat: defaultHeartbeat,
	}
	for i := range opts {
		opts[i](n)
	}
	if n.ID == "" {
		randUUID, _ := uuid.NewUUID()
		n.ID = randUUID.String()
	}
	if n.transport == nil {
		return nil, fmt.Errorf("transport can't be nil")
	}
	n.hearthBeat()
	return n, nil
}

func (n *Node) hearthBeat() {
	n.wg.Add(1)
	go func() {
		defer n.wg.Done()

		timer := time.NewTicker(n.heartbeat)
		defer timer.Stop()

		for {
			select {
			case <-n.ctx.Done():
				return
			case <-timer.C:
				err := n.Join()
				if err != nil {
					// LOG
				}
			}
		}
	}()
}
func (n *Node) Close() error {
	n.cancel()
	n.wg.Wait()
	return n.Leave()
}

func (n *Node) Join() error {
	return n.transport.Join(n.ID)
}

func (n *Node) Leave() error {
	return n.transport.Leave(n.ID)
}
