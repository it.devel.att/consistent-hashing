package redis

import (
	"consistent-hashing/pkg/transports"
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

const (
	joinChannel  = "JoinNodes"
	leaveChannel = "LeaveNodes"
)

type NodeTransport struct {
	client     *redis.Client
	expiration time.Duration
}

type NodeOption func(*NodeTransport)

var _ transports.Node = (*NodeTransport)(nil)

func WithExpiration(d time.Duration) NodeOption {
	return func(nt *NodeTransport) {
		nt.expiration = d
	}
}

func NewTransport(client *redis.Client, options ...NodeOption) *NodeTransport {
	nt := &NodeTransport{
		client:     client,
		expiration: time.Second * 15,
	}
	for i := range options {
		options[i](nt)
	}
	return nt
}

func generateKey(id string) string {
	return joinChannel + ":" + id
}

func (t *NodeTransport) Join(id string) error {
	ctx := context.Background()
	pipe := t.client.Pipeline()

	pipe.SetEx(ctx, generateKey(id), "", t.expiration)
	pipe.Publish(ctx, joinChannel, id)
	_, err := pipe.Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (t *NodeTransport) Leave(id string) error {
	ctx := context.Background()
	pipe := t.client.Pipeline()

	pipe.Del(ctx, generateKey(id))
	pipe.Publish(ctx, leaveChannel, id)
	_, err := pipe.Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}
