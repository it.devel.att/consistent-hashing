package redis

import (
	"consistent-hashing/pkg/transports"
	"context"
	"fmt"
	"strings"
	"sync"

	"github.com/redis/go-redis/v9"
)

type ObserverTransport struct {
	ctx    context.Context
	cancel context.CancelFunc
	wg     *sync.WaitGroup

	client   *redis.Client
	joinsCh  chan string
	leavesCh chan string
}

var _ transports.Observer = (*ObserverTransport)(nil)

func NewObserver(client *redis.Client) *ObserverTransport {
	ctx, cancel := context.WithCancel(context.Background())
	t := &ObserverTransport{
		ctx:      ctx,
		cancel:   cancel,
		wg:       &sync.WaitGroup{},
		client:   client,
		joinsCh:  make(chan string, 10),
		leavesCh: make(chan string, 10),
	}
	t.run()
	return t
}

func (t *ObserverTransport) Nodes() ([]string, error) {
	ctx := context.Background()
	// Call keys it's not the best solution
	cmd := t.client.Keys(ctx, joinChannel+":*")
	result, err := cmd.Result()
	if err != nil {
		return nil, err
	}
	nodeIDs := make([]string, 0, len(result))
	for _, channelNode := range result {
		node := strings.Split(channelNode, ":")[1]
		nodeIDs = append(nodeIDs, node)
	}
	return nodeIDs, nil
}

func (t *ObserverTransport) Joins() chan string {
	return t.joinsCh
}

func (t *ObserverTransport) Leaves() chan string {
	return t.leavesCh
}

func (t *ObserverTransport) Close() {
	t.cancel()
	t.wg.Wait()
}

func (t *ObserverTransport) run() {
	t.wg.Add(1)

	go func() {
		defer t.wg.Done()
		joinsSub := t.listenJoins(t.ctx)
		leaveSub := t.listenLeaves(t.ctx)

		joinsSubChannel := joinsSub.Channel()
		leavesSubChannel := leaveSub.Channel()

		for {
			select {
			case <-t.ctx.Done():
				if err := leaveSub.Close(); err != nil {
					fmt.Println("err while try to close leaveSub subscription")
				}
				if err := joinsSub.Close(); err != nil {
					fmt.Println("err while try to close joinsSub subscription")
				}
				return
			case msg := <-joinsSubChannel:
				// TOOD non blocking manner
				t.joinsCh <- msg.Payload
			case msg := <-leavesSubChannel:
				// TOOD non blocking manner
				t.leavesCh <- msg.Payload
			}
		}
	}()
}

func (t *ObserverTransport) listenJoins(ctx context.Context) *redis.PubSub {
	pubSub := t.client.Subscribe(ctx, joinChannel)
	return pubSub
}

func (t *ObserverTransport) listenLeaves(ctx context.Context) *redis.PubSub {
	pubSub := t.client.Subscribe(ctx, leaveChannel)
	return pubSub
}
