package transports

type Observer interface {
	Nodes() ([]string, error)
	Joins() chan string
	Leaves() chan string
}

type Node interface {
	Join(string) error
	Leave(string) error
}
