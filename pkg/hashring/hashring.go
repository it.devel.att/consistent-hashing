package hashring

import (
	"fmt"
	"hash/crc32"
	"slices"
	"sync"
)

type Ring struct {
	mu          sync.RWMutex
	nodes       map[uint32]string
	sortedNodes []uint32
	hasher      func([]byte) uint32
}

func New(initialNodes []string) *Ring {
	r := &Ring{
		mu:          sync.RWMutex{},
		nodes:       make(map[uint32]string),
		sortedNodes: make([]uint32, 0),
		hasher:      crc32.ChecksumIEEE,
	}
	r.Set(initialNodes)
	return r
}

func (r *Ring) Set(nodes []string) {
	if len(nodes) == 0 {
		return
	}

	r.mu.Lock()
	defer r.mu.Unlock()

	for i := range nodes {
		node := nodes[i]
		nodeID := r.hasher([]byte(node))
		if _, ok := r.nodes[nodeID]; ok {
			continue
		}
		r.nodes[nodeID] = node
		r.sortedNodes = append(r.sortedNodes, nodeID)
	}
	slices.Sort(r.sortedNodes)
}

func (r *Ring) Join(node string) {
	r.mu.Lock()
	defer r.mu.Unlock()

	nodeID := r.hasher([]byte(node))
	if _, ok := r.nodes[nodeID]; ok {
		return
	}
	r.nodes[nodeID] = node
	r.sortedNodes = append(r.sortedNodes, nodeID)
	slices.Sort(r.sortedNodes)
}

func (r *Ring) Delete(node string) {
	r.mu.Lock()
	defer r.mu.Unlock()

	nodeID := r.hasher([]byte(node))
	if _, ok := r.nodes[nodeID]; !ok {
		return
	}
	index, ok := slices.BinarySearch(r.sortedNodes, nodeID)
	if !ok {
		return
	}
	delete(r.nodes, nodeID)
	// Check overflow
	r.sortedNodes = append(r.sortedNodes[:index], r.sortedNodes[index+1:]...)
}

func (r *Ring) GetNode(key []byte) string {
	r.mu.RLock()
	defer r.mu.RUnlock()
	if len(r.sortedNodes) == 0 {
		return ""
	}

	keyHash := r.hasher(key)

	nodeID := 0
	// probably change to binary search
	// slices.BinarySearchFunc[]()
	for i := range r.sortedNodes {
		if r.sortedNodes[i] > keyHash {
			nodeID = i
			break
		}
	}
	return r.nodes[r.sortedNodes[nodeID]]
}

func (r *Ring) print() {
	for i := range r.sortedNodes {
		fmt.Printf("[%s|%v]", r.nodes[r.sortedNodes[i]], r.sortedNodes[i])
		fmt.Print(" ")
	}
	fmt.Println()
}
