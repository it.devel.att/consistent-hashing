package hashring

import (
	"slices"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_add(t *testing.T) {
	type testCase struct {
	}

	hr := New(nil)

	newNodes := make([]string, 10)
	for i := range 10 {
		newNodes[i] = strconv.Itoa(i)
	}

	for i := range newNodes {
		hr.Join(newNodes[i])
	}

	assert.Equal(t, 10, len(hr.nodes))
	assert.Equal(t, 10, len(hr.sortedNodes))
	assert.True(t, slices.IsSorted(hr.sortedNodes))

	node := hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "7", node)
	hr.print()

	hr.Join("11")
	node = hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "7", node)
	hr.print()

	hr.Join("12")
	node = hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "7", node)
	hr.print()

	hr.Join("13")
	node = hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "7", node)
	hr.print()

	hr.Join("14")
	node = hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "7", node)
	hr.print()

	hr.Delete("7")
	node = hr.GetNode([]byte(`123xyz`))
	assert.Equal(t, "3", node)
	hr.print()
}
